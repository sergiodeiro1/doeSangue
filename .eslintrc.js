module.exports = {
  root: true,
  extends: '@react-native-community',
  rules: {
    // disable
    'react-hooks/exhaustive-deps': 'off',
  },
};
