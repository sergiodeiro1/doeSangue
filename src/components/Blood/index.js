import React from 'react';
import {FlatList} from 'react-native';
import styled from 'styled-components/native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

const Container = styled.View`
  margin-top: ${(props) => props.marginTop}px;
  margin-left: ${(props) => props.marginLeft}px;
  margin-right: ${(props) => props.marginRigth}px;
  margin-bottom: ${(props) => props.marginBottom}px;
`;

const Content = styled.View`
  flex: 1;
  margin-top: 16px;
  flex-wrap: wrap;
  flex-direction: row;
`;

const Label = styled.Text`
  font-size: ${(props) => props.fontSize || wp('5%')}px;
  color: ${(props) => props.color || props.theme.thirdColor};
  font-weight: bold;
  margin-left: ${(props) => props.marginLeft}px;
`;

const ContainerList = styled.TouchableOpacity`
  margin-right: 16px;
  width: 54px;
  height: 54px;
  border-radius: 29px;
  border-color: ${(props) =>
    props.selected ? props.theme.primaryColor : props.theme.eighthColor};
  background-color: ${(props) =>
    props.selected ? props.theme.primaryColor : 'transparent'};
  border-width: 1px;
  align-items: center;
  justify-content: center;
  margin-top: 8px;
  margin-bottom: 8px;
`;

const TitleList = styled.Text`
  font-size: ${(props) => props.fontSize || wp('5%')}px;
  color: ${(props) =>
    props.color || props.selected
      ? props.theme.secondColor
      : props.theme.eighthColor};
`;

const data = ['A+', 'A-', 'B+', 'B-', 'O+', 'O-', 'AB+', 'AB-'];

const renderItem = (item, index, props) => {
  return (
    <ContainerList
      disabled={!props.editable}
      key={index}
      selected={props.value === item ? true : false}
      onPress={() => props.onPress(item)}>
      <TitleList selected={props.value === item ? true : false}>
        {item}
      </TitleList>
    </ContainerList>
  );
};

const Blood = (props) => (
  <Container
    marginLeft={props.marginLeft}
    marginRigth={props.marginRigth}
    marginTop={props.marginTop}
    marginBottom={props.marginBottom}>
    <Label marginLeft={props.marginLeft}>{props.label}</Label>
    <Content>
      {data.map((value, index) => {
        return renderItem(value, index, props);
      })}
    </Content>
  </Container>
);

Blood.defaultProps = {
  marginBottom: 8,
  marginTop: 8,
  marginRigth: 8,
  marginLeft: 8,
  value: 'A+',
  editable: true,
  onPress: () => {},
};

export default Blood;
