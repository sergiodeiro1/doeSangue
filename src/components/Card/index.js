import React from 'react';
import styled from 'styled-components/native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import Title from '../Title';
import {theme} from '../../resources';

const Container = styled.View`
  width: ${wp('43.5%')}px;
  height: ${wp('43.5%')}px;
  border-radius: 16px;
  overflow: hidden;
  margin-bottom: ${wp('4%')}px;
`;

const Image = styled.Image`
  width: 100%;
  height: 100%;
`;

const SectionText = styled.View`
  position: absolute;
  justify-content: center;
  left: 0;
  right: 0;
  bottom: 0;
  top: 0;
`;

const Card = (props) => {
  return (
    <Container>
      <Image source={props.sourceImage} resizeMode="contain" />
      <SectionText>
        <Title color={theme.secondColor}>{props.label}</Title>
      </SectionText>
    </Container>
  );
};

export default Card;
