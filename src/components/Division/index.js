import styled from 'styled-components/native';

export default styled.View`
  border-bottom-width: ${(props) => props.height || 2}px;
  border-color: ${(props) => props.color || props.theme.secondColor};
  margin: 4px 0px;
  width: ${(props) => props.width || '100%'};
`;
