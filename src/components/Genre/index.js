import React from 'react';
import styled from 'styled-components/native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {images, theme} from '../../resources';

const Container = styled.View`
  margin-top: ${(props) => props.marginTop}px;
  margin-left: ${(props) => props.marginLeft}px;
  margin-right: ${(props) => props.marginRigth}px;
  margin-bottom: ${(props) => props.marginBottom}px;
`;

const Content = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin-top: 8px;
`;

const Touch = styled.TouchableOpacity`
  align-items: center;
`;

const Icon = styled.Image`
  height: 60px;
  width: 60px;
  tint-color: ${(props) => props.tintColor};
`;

const Division = styled.View`
  height: 40px;
  border-color: ${(props) => props.theme.tenthColor};
  border-left-width: 2px;
  margin-left: 20px;
  margin-right: 20px;
`;

const Label = styled.Text`
  font-size: ${(props) => props.fontSize || wp('5%')}px;
  color: ${(props) => props.color || props.theme.thirdColor};
  font-weight: bold;
  margin-left: ${(props) => props.marginLeft}px;
`;

const Title = styled.Text`
  font-size: ${(props) => props.fontSize || wp('5%')}px;
  color: ${(props) => props.color || props.theme.eighthColor};
  text-align: center;
  margin-top: 8px;
`;

const Genre = (props) => {
  return (
    <Container
      marginLeft={props.marginLeft}
      marginRigth={props.marginRigth}
      marginTop={props.marginTop}
      marginBottom={props.marginBottom}>
      <Label marginLeft={props.marginLeft}>{props.label}</Label>
      <Content>
        <Touch onPress={() => props.onPress('M')}>
          <Icon
            tintColor={
              props.value === 'M' ? theme.primaryColor : theme.eighthColor
            }
            resizeMode="contain"
            source={images.man}
          />
          <Title
            color={
              props.value === 'M' ? theme.primaryColor : theme.eighthColor
            }>
            Masculino
          </Title>
        </Touch>
        <Division />
        <Touch onPress={() => props.onPress('F')}>
          <Icon
            tintColor={
              props.value === 'F' ? theme.primaryColor : theme.eighthColor
            }
            resizeMode="contain"
            source={images.woman}
          />
          <Title
            color={
              props.value === 'F' ? theme.primaryColor : theme.eighthColor
            }>
            Feminino
          </Title>
        </Touch>
      </Content>
    </Container>
  );
};

Genre.defaultProps = {
  marginBottom: 8,
  marginTop: 8,
  marginRigth: 8,
  marginLeft: 8,
  value: 'M',
  onPress: () => {},
};

export default Genre;
