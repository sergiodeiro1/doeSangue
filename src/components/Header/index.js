import React from 'react';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import styled from 'styled-components/native';
import {images, theme} from '../../resources';
import Title from '../Title';

const Container = styled.View`
  flex-direction: row;
  padding-top: 6px;
  padding-bottom: 6px;
  padding-left: ${(props) => props.paddingHorizontal || wp('5%')}px;
  padding-right: ${(props) => props.paddingHorizontal || wp('5%')}px;
  background-color: ${(props) =>
    props.backgroundColor || props.theme.secondColor};
  align-items: center;
`;

const SectionButton = styled.View`
  flex-direction: row;
  align-items: center;
`;

const SectionLogo = styled.View`
  flex: 1;
  justify-content: center;
  margin-left: ${(props) => (props.isBack ? `-${wp('6%')}px;` : '0px')};
`;

const Image = styled.Image`
  width: ${wp('6%')}px;
  tint-color: ${(props) => props.titleColor};
`;

const Touch = styled.TouchableOpacity`
  width: ${wp('10%')}px;
  align-items: center;
  justify-content: center;
  height: ${wp('10%')}px;
`;

const HeaderBack = (props) => {
  return (
    <Container backgroundColor={props.backgroundColor}>
      {props.isBack ? (
        <SectionButton>
          <Touch {...props} activeOpacity={0.4} onPress={props.onPress}>
            <Image
              source={images.back}
              titleColor={props.titleColor || theme.primaryColor}
              resizeMode="contain"
            />
          </Touch>
        </SectionButton>
      ) : (
        <SectionButton />
      )}
      <SectionLogo>
        <Title align="left" color={props.titleColor || theme.thirdColor}>
          {props.title}
        </Title>
      </SectionLogo>
    </Container>
  );
};

HeaderBack.defaultProps = {
  isBack: true,
};

export default HeaderBack;
