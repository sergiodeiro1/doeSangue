import styled, {css} from 'styled-components/native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

const Title = styled.Text`
  color: ${(props) => props.color || props.theme.primaryColor};
  text-align: ${(props) => props.align || 'center'};

  ${(props) =>
    props.size === 'small' &&
    css`
      font-size: ${wp('4%')}px;
    `}

    ${(props) =>
      props.size === 'medium' &&
      css`
        font-size: ${wp('6%')}px;
      `}

      ${(props) =>
        props.size === 'large' &&
        css`
          font-size: ${wp('8%')}px;
        `}
  margin-top: ${(props) => props.marginTop}px;
  margin-left: ${(props) => props.marginLeft}px;
  margin-right: ${(props) => props.marginRigth}px;
  margin-bottom: ${(props) => props.marginBottom}px;
  font-weight: ${(props) => props.weight || 'normal'};
`;

Title.defaultProps = {
  marginBottom: 8,
  marginTop: 8,
  marginRigth: 8,
  marginLeft: 8,
  size: 'medium',
};

export default Title;
