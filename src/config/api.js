import axios from 'axios';

const enviroments = {
  BASE_URL: 'https://api/v1/',
  API_TOKEN: 'CHAVE_API',
};

export default axios.create({
  baseURL: enviroments.BASE_URL,
  headers: {
    Authorization: enviroments.API_TOKEN,
    'Content-Type': 'application/json',
  },
  validateStatus: (status) => status <= 500,
});
