import {combineReducers} from 'redux';
import auth from './auth';
import solicitation from './solicitation';
import user from './user';

export default combineReducers({
  auth,
  solicitation,
  user,
});
