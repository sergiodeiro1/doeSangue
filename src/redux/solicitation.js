import firestore from '@react-native-firebase/firestore';
import firebase from '@react-native-firebase/app';
import AsyncStorage from '@react-native-community/async-storage';
import {persistReducer} from 'redux-persist';

// Actions

const SET_SOLICITATION_REQUEST = 'dooApp/solicitation/SET_SOLICITATION_REQUEST';
const SET_SOLICITATION_SUCCESS = 'dooApp/solicitation/SET_SOLICITATION_SUCCESS';
const SET_SOLICITATION_FAILURE = 'dooApp/solicitation/SET_SOLICITATION_FAILURE';

const GET_SOLICITATIONS_REQUEST =
  'dooApp/solicitation/GET_SOLICITATIONS_REQUEST';
const GET_SOLICITATIONS_SUCCESS =
  'dooApp/solicitation/GET_SOLICITATIONS_SUCCESS';
const GET_SOLICITATIONS_FAILURE =
  'dooApp/solicitation/GET_SOLICITATIONS_FAILURE';

const GET_SOLICITATION_REQUEST = 'dooApp/solicitation/GET_SOLICITATION_REQUEST';
const GET_SOLICITATION_SUCCESS = 'dooApp/solicitation/GET_SOLICITATION_SUCCESS';
const GET_SOLICITATION_FAILURE = 'dooApp/solicitation/GET_SOLICITATION_FAILURE';

const COUNT_SOLICITATION = 'dooApp/solicitation/COUNT_SOLICITATION';

const UPDATE_SOLICITATION_REQUEST =
  'dooApp/solicitation/UPDATE_SOLICITATION_REQUEST';
const UPDATE_SOLICITATION_SUCCESS =
  'dooApp/solicitation/UPDATE_SOLICITATION_SUCCESS';
const UPDATE_SOLICITATION_FAILURE =
  'dooApp/solicitation/UPDATE_SOLICITATION_FAILURE';

const DELETE_SOLICITATION_REQUEST =
  'dooApp/solicitation/DELETE_SOLICITATION_REQUEST';
const DELETE_SOLICITATION_SUCCESS =
  'dooApp/solicitation/DELETE_SOLICITATION_SUCCESS';
const DELETE_SOLICITATION_FAILURE =
  'dooApp/solicitation/DELETE_SOLICITATION_FAILURE';

const initialState = {
  solicitation: {},
  solicitations: [],
  solicitationsUser: [],
  updateSolicitation: {},
  countDataSolicitations: 0,
};

// Reducer
function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case SET_SOLICITATION_REQUEST:
      return {
        ...state,
        isFetchSetSolicitation: true,
        isSuccessSetSolicitation: false,
        isErrorSetSolicitation: false,
      };
    case SET_SOLICITATION_SUCCESS:
      return {
        ...state,
        isFetchSetSolicitation: false,
        isSuccessSetSolicitation: true,
        isErrorSetSolicitation: false,
        solicitation: action.payload,
      };
    case SET_SOLICITATION_FAILURE:
      return {
        ...state,
        isFetchSetSolicitation: false,
        isSuccessSetSolicitation: false,
        isErrorSetSolicitation: true,
        message: action.message,
      };

    case GET_SOLICITATIONS_REQUEST:
      return {
        ...state,
        isFetchGetSolicitations: true,
        isSuccessGetSolicitations: false,
        isErrorGetSolicitations: false,
      };
    case GET_SOLICITATIONS_SUCCESS:
      return {
        ...state,
        isFetchGetSolicitations: false,
        isSuccessGetSolicitations: true,
        isErrorGetSolicitations: false,
        solicitations: action.payload,
      };
    case GET_SOLICITATIONS_FAILURE:
      return {
        ...state,
        isFetchGetSolicitations: false,
        isSuccessGetSolicitations: false,
        isErrorGetSolicitations: true,
        message: action.message,
      };

    case GET_SOLICITATION_REQUEST:
      return {
        ...state,
        isFetchGetSolicitation: true,
        isSuccessGetSolicitation: false,
        isErrorGetSolicitation: false,
      };
    case GET_SOLICITATION_SUCCESS:
      return {
        ...state,
        isFetchGetSolicitation: false,
        isSuccessGetSolicitation: true,
        isErrorGetSolicitation: false,
        solicitationsUser: action.payload,
      };
    case GET_SOLICITATION_FAILURE:
      return {
        ...state,
        isFetchGetSolicitation: false,
        isSuccessGetSolicitation: false,
        isErrorGetSolicitation: true,
        message: action.message,
      };

    case UPDATE_SOLICITATION_REQUEST:
      return {
        ...state,
        isFetchUpdateSolicitation: true,
        isSuccessUpdateSolicitation: false,
        isErrorUpdateSolicitation: false,
      };
    case UPDATE_SOLICITATION_SUCCESS:
      return {
        ...state,
        isFetchUpdateSolicitation: false,
        isSuccessUpdateSolicitation: true,
        isErrorUpdateSolicitation: false,
        updateSolicitation: action.payload,
      };
    case UPDATE_SOLICITATION_FAILURE:
      return {
        ...state,
        isFetchUpdateSolicitation: false,
        isSuccessUpdateSolicitation: false,
        isErrorUpdateSolicitation: true,
        message: action.message,
      };

    case DELETE_SOLICITATION_REQUEST:
      return {
        ...state,
        isFetchDeleteSolicitation: true,
        isSuccessDeleteSolicitation: false,
        isErrorDeleteSolicitation: false,
      };
    case DELETE_SOLICITATION_SUCCESS:
      return {
        ...state,
        isFetchDeleteSolicitation: false,
        isSuccessDeleteSolicitation: true,
        isErrorDeleteSolicitation: false,
        deleteSolicitation: action.payload,
      };
    case DELETE_SOLICITATION_FAILURE:
      return {
        ...state,
        isFetchDeleteSolicitation: false,
        isSuccessDeleteSolicitation: false,
        isErrorDeleteSolicitation: true,
        message: action.message,
      };

    case COUNT_SOLICITATION:
      return {
        ...state,
        countSolicitations: action.payload,
      };

    default:
      return state;
  }
}

const persistConfig = {
  key: 'solicitation',
  storage: AsyncStorage,
  blacklist: [
    'message',
    'isSuccessSetSolicitation',
    'isErrorSetSolicitation',
    'isFetchSetSolicitation',
    'isSuccessSetSolicitations',
    'isErrorSetSolicitations',
    'isFetchSetSolicitations',
    'isFetchUpdateSolicitation',
    'isSuccessUpdateSolicitation',
    'isErrorUpdateSolicitation',
    'isFetchDeleteSolicitation',
    'isSuccessDeleteSolicitation',
    'isErrorDeleteSolicitation',
  ],
};

export const setSolicitation = ({address, name, cellphone, bloodType}) => (
  dispatch,
  getStates,
) => {
  const {auth = {user: {}}} = getStates().auth;

  dispatch({type: SET_SOLICITATION_REQUEST});

  if (!address || !name || !bloodType) {
    return dispatch({
      type: SET_SOLICITATION_FAILURE,
      message: 'Verifique os dados digitados',
    });
  }

  const solicitation = firestore().collection('solicitations');
  solicitation
    .add({
      userId: auth.user.uid,
      name,
      address,
      cellphone,
      bloodType,
    })
    .then((dataUser) => {
      return dispatch({
        type: SET_SOLICITATION_SUCCESS,
        payload: {
          userId: auth.user.uid,
          name,
          address,
          cellphone,
          bloodType,
        },
      });
    })
    .catch((error) => {
      return dispatch({
        type: SET_SOLICITATION_FAILURE,
        message: 'Erro ao criar solicitação, tente novamente',
      });
    });
};

export const getSolicitations = () => (dispatch, getStates) => {
  dispatch({type: GET_SOLICITATIONS_REQUEST});

  const solicitation = firestore().collection('solicitations');

  solicitation
    .get()
    .then((dataResponse) => {
      const data = [];

      dataResponse.forEach((item) => {
        const dataItem = item.data();
        dataItem.id = item.id;
        data.push(dataItem);
      });

      return dispatch({
        type: GET_SOLICITATIONS_SUCCESS,
        payload: data,
      });
    })
    .catch((error) => {
      return dispatch({
        type: GET_SOLICITATIONS_FAILURE,
        message: 'Erro ao buscar solicitação, tente novamente',
      });
    });
};

export const getSolicitationsById = () => (dispatch, getStates) => {
  const {auth = {user: {}}} = getStates().auth;

  dispatch({type: GET_SOLICITATION_REQUEST});

  const solicitation = firestore().collection('solicitations');

  solicitation
    .where('userId', '==', auth.user.uid)
    .get()
    .then((dataResponse) => {
      const data = [];

      dataResponse.forEach((item) => {
        const dataItem = item.data();
        dataItem.id = item.id;
        data.push(dataItem);
      });

      return dispatch({
        type: GET_SOLICITATION_SUCCESS,
        payload: data,
      });
    })
    .catch((error) => {
      return dispatch({
        type: GET_SOLICITATION_FAILURE,
        message: 'Erro ao buscar solicitação, tente novamente',
      });
    });
};

export const updateSolicitation = ({
  address,
  name,
  cellphone,
  bloodType,
  id,
}) => (dispatch) => {
  dispatch({type: UPDATE_SOLICITATION_REQUEST});

  if (!address || !name || !bloodType) {
    return dispatch({
      type: UPDATE_SOLICITATION_FAILURE,
      message: 'Verifique os dados digitados',
    });
  }

  const solicitation = firestore().collection('solicitations');
  solicitation
    .doc(id)
    .update({
      address,
      name,
      cellphone,
      bloodType,
    })
    .then(() => {
      return dispatch({
        type: UPDATE_SOLICITATION_SUCCESS,
        payload: {address, name, cellphone, bloodType},
      });
    })
    .catch(() => {
      return dispatch({
        type: UPDATE_SOLICITATION_FAILURE,
        message: 'Erro ao atualizar solicitação, tente novamente',
      });
    });
};

export const deleteSolicitation = ({id}) => (dispatch, getStates) => {
  dispatch({type: DELETE_SOLICITATION_REQUEST});

  const solicitation = firestore().collection('solicitations');
  solicitation
    .doc(id)
    .delete()
    .then(() => {
      return dispatch({
        type: DELETE_SOLICITATION_SUCCESS,
        payload: {id},
      });
    })
    .catch((error) => {
      return dispatch({
        type: DELETE_SOLICITATION_FAILURE,
        message: 'Erro ao deletar solicitação, tente novamente',
      });
    });
};

export const countSolicitations = () => (dispatch, getStates) => {
  return dispatch({
    type: COUNT_SOLICITATION,
    payload: 0,
  });
};

export default persistReducer(persistConfig, reducer);
