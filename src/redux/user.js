import firestore from '@react-native-firebase/firestore';
import AsyncStorage from '@react-native-community/async-storage';
import {persistReducer} from 'redux-persist';

// Actions
const GET_USERS_REQUEST = 'dooApp/user/GET_USERS_REQUEST';
const GET_USERS_SUCCESS = 'dooApp/user/GET_USERS_SUCCESS';
const GET_USERS_FAILURE = 'dooApp/user/GET_USERS_FAILURE';

const initialState = {
  users: [],
};

// Reducer
function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case GET_USERS_REQUEST:
      return {
        ...state,
        isFetchGetUsers: true,
        isSuccessGetUsers: false,
        isErrorGetUsers: false,
      };
    case GET_USERS_SUCCESS:
      return {
        ...state,
        isFetchGetUsers: false,
        isSuccessGetUsers: true,
        isErrorGetUsers: false,
        users: action.payload,
      };
    case GET_USERS_FAILURE:
      return {
        ...state,
        isFetchGetUsers: false,
        isSuccessGetUsers: false,
        isErrorGetUsers: true,
        message: action.message,
      };

    default:
      return state;
  }
}

const persistConfig = {
  key: 'user',
  storage: AsyncStorage,
  blacklist: [
    'message',
    'isSuccessGetUsers',
    'isFetchGetUsers',
    'isErrorGetUsers',
  ],
};

export const getUsers = () => (dispatch, getStates) => {
  dispatch({type: GET_USERS_REQUEST});

  const solicitation = firestore().collection('users');
  solicitation
    .get()
    .then((dataResponse) => {
      const data = [];

      dataResponse.forEach((item) => {
        const dataItem = item.data();
        dataItem.id = item.id;
        data.push(dataItem);
      });

      return dispatch({
        type: GET_USERS_SUCCESS,
        payload: data,
      });
    })
    .catch((error) => {
      return dispatch({
        type: GET_USERS_FAILURE,
        message: 'Erro ao buscar solicitação, tente novamente',
      });
    });
};

export default persistReducer(persistConfig, reducer);
