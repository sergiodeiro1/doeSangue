const logo = require('./logo.png');
const back = require('./back.png');
const next = require('./next.png');
const man = require('./man.png');
const woman = require('./woman.png');
const exit = require('./exit.png')

export default {logo, next, back, man, woman, exit};
