const theme = {
  primaryColor: '#e60023',
  secondColor: '#FFFFFF',
  thirdColor: '#000000',
  fourthColor: '#e60023',
  fifthColor: '#FF4D4D',
  sixthColor: '#D8D8D8',
  seventhColor: '#616161',
  eighthColor: '#A2A2A2',
  ninethColor: '#F6F6F6',
  tenthColor: '#c9c9c9',
};

export default theme;
