import React from 'react';
import {Table, Row, Rows} from 'react-native-table-component';
import {ScrollView, StyleSheet} from 'react-native';
import {Container, Content, Header, Title, Button} from '../../components';
import {Section, SectionButton} from './styles';
import {theme} from '../../resources';

export default ({navigation, route}) => {
  const tableHeadDonate = ['Tipo Sanguíneo(doador)', 'Pode doar para'];
  const tableDataDonate = [
    ['A+', 'A+ e AB+'],
    ['A-', 'A+, A-, AB+ e AB-'],
    ['B+', 'B+ e AB+'],
    ['B-', 'B+, B-, AB+ e AB-'],
    ['AB+', 'AB+'],
    ['AB-', 'AB+ e AB-'],
    ['O+', 'A+, B+, O+ e AB+'],
    ['O-', 'A+, A-, B+, B-, AB+, AB-, O+ e O-'],
  ];

  const tableHeadReceptor = ['Tipo Sanguíneo(receptor)', 'Pode receber de'];
  const tableDataReceptor = [
    ['A+', 'A+, A-, O+ e O-'],
    ['A-', 'A- e O-'],
    ['B+', 'B+, B-, O+ e O-'],
    ['B-', 'B- e O-'],
    ['AB+', 'A+, A-, B+, B-, AB+, AB-, O+ e O-'],
    ['AB-', 'A-, B-, AB- e O-'],
    ['O+', 'O+ e O-'],
    ['O-', 'O-'],
  ];

  return (
    <>
      <Container>
        <Header onPress={() => navigation.goBack()} title="Informações" />
        <ScrollView>
          <Content>
            <Section>
              <Title align="left" marginLeft={0}>
                Doador
              </Title>
              <Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>
                <Row
                  data={tableHeadDonate}
                  style={styles.head}
                  textStyle={styles.text}
                />
                <Rows data={tableDataDonate} textStyle={styles.text} />
              </Table>
            </Section>
            <Section>
              <Title align="left" marginLeft={0}>
                Receptor
              </Title>
              <Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>
                <Row
                  data={tableHeadReceptor}
                  style={styles.head}
                  textStyle={styles.text}
                />
                <Rows data={tableDataReceptor} textStyle={styles.text} />
              </Table>
            </Section>
          </Content>
        </ScrollView>
      </Container>
    </>
  );
};

const styles = StyleSheet.create({
  container: {flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff'},
  head: {height: 40, backgroundColor: '#f1f8ff'},
  text: {margin: 6},
});
