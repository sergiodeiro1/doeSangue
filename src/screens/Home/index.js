import React, { useEffect, useState, useRef } from 'react';
import DropdownAlert from 'react-native-dropdownalert';
import { useDispatch, useSelector } from 'react-redux';
import { Title, Button, Logo } from '../../components';
import { theme, images } from '../../resources';
import {
  ContainerGradient,
  SectionInfo,
  SectionInfoValues,
  SectionInfoButton,
  Content,
  Header,
  Touch,
  Wrapper,
  WrapperImage,
} from './styles';

import { getSolicitations, getSolicitationsById } from '../../redux/solicitation';
import { getUsers } from '../../redux/user';
import { logout } from '../../redux/auth'

export default ({ navigation }) => {
  const dropDownAlertRef = useRef();
  const dispatch = useDispatch();

  const { solicitations, solicitationsUser } = useSelector(
    (state) => state.solicitation,
  );
  const { users } = useSelector((state) => state.user);

  useEffect(() => {
    dispatch(getSolicitations());
    dispatch(getSolicitationsById());
    dispatch(getUsers());
  }, []);

  const onLogout = () => {
    navigation.navigate('Login');
    dispatch(logout());
  }

  return (
    <>
      <ContainerGradient colors={[theme.fourthColor, theme.fifthColor]}>
        <Content>
          <Header>
            <Wrapper>
              <Logo height="70" />
            </Wrapper>
            <Touch onPress={onLogout}>
              <WrapperImage source={images.exit}/>
            </Touch>
          </Header>
          <SectionInfo>
            <SectionInfoValues>
              <Title marginTop={0} color={theme.secondColor} marginBottom={0}>
                {users.length}
              </Title>
              <Title marginTop={0} color={theme.secondColor} size="small">
                Doadores
              </Title>
            </SectionInfoValues>
            <SectionInfoButton>
              <Button
                onPress={() => navigation.navigate('Request')}
                color={theme.primaryColor}
                type="rounded"
                backgroundColor={theme.secondColor}
                title="Solicitar Doador"
              />
            </SectionInfoButton>
          </SectionInfo>

          <SectionInfo>
            <SectionInfoValues>
              <Title marginTop={0} color={theme.secondColor} marginBottom={0}>
                {solicitations.length}
              </Title>
              <Title marginTop={0} color={theme.secondColor} size="small">
                Solicitações
              </Title>
            </SectionInfoValues>
            <SectionInfoButton>
              <Button
                onPress={() => navigation.navigate('Donate')}
                color={theme.primaryColor}
                type="rounded"
                backgroundColor={theme.secondColor}
                title="Doar"
              />
            </SectionInfoButton>
          </SectionInfo>

          <SectionInfo>
            <SectionInfoValues>
              <Title marginTop={0} color={theme.secondColor} marginBottom={0}>
                {solicitationsUser.length}
              </Title>
              <Title marginTop={0} color={theme.secondColor} size="small">
                Solicitações
              </Title>
            </SectionInfoValues>
            <SectionInfoButton>
              <Button
                onPress={() => navigation.navigate('Solicitations')}
                color={theme.primaryColor}
                type="rounded"
                backgroundColor={theme.secondColor}
                title="Solicitações"
              />
            </SectionInfoButton>
          </SectionInfo>
        </Content>
      </ContainerGradient>
      <DropdownAlert ref={dropDownAlertRef} />
    </>
  );
};
