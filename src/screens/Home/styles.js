import styled from 'styled-components/native';
import LinearGradient from 'react-native-linear-gradient';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

export const ContainerGradient = styled(LinearGradient)`
  flex: 1;
  justify-content: center;
  align-items: center;
  padding-top: ${hp('8%')}px;
`;

export const SectionInfo = styled.View`
  flex-direction: row;
  background-color: rgba(255, 255, 255, 0.3);
  align-items: center;
  height: ${hp('16%')}px;
  border-radius: 16px;
  margin-bottom: 16px;
  padding: 8px;
`;

export const SectionInfoValues = styled.View`
  flex: 1;
`;
export const SectionInfoButton = styled.View`
  flex: 2;
`;

export const Content = styled.View`
  margin-top: ${hp('8%')}px;
  height: ${hp('100%')}px;
  width: ${wp('100%')}px;
  padding-left: ${(props) => props.paddingHorizontal || wp('4.9%')}px;
  padding-right: ${(props) => props.paddingHorizontal || wp('4.9%')}px;
`;

export const Header = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin-bottom: ${hp('4%')}px;
`;

export const Touch = styled.TouchableOpacity`
`

export const WrapperImage = styled.ImageBackground`
  width: 32px;
  margin-top: ${wp(8)}px;
  height: 32px;
`

export const Wrapper = styled.View`
width: ${wp('70%')}px;
display: flex;
align-items: flex-end;
justify-content: flex-end;
`
