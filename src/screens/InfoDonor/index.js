import React from 'react';
import {ScrollView} from 'react-native';
import VMasker from 'vanilla-masker';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {Container, Content, Input, Header, Blood} from '../../components';
import {Section} from './styles';

export default ({navigation, route}) => {
  return (
    <>
      <Container>
        <Header onPress={() => navigation.navigate('Donate')} title="Doar" />
        <ScrollView>
          <Content>
            <KeyboardAwareScrollView>
              <Section>
                <Input
                  editable={false}
                  value={route.params.selectedDonate.name}
                  label="Nome"
                  autoCapitalize="none"
                  placeholder="Nome do receptor"
                />
                <Input
                  editable={false}
                  value={route.params.selectedDonate.address}
                  label="Local da doação"
                  autoCapitalize="none"
                  placeholder="Rua x, número 34"
                />
                <Input
                  editable={false}
                  value={VMasker.toPattern(
                    route.params.selectedDonate.cellphone,
                    '(99) 99999-9999',
                  )}
                  label="Celular"
                  autoCapitalize="none"
                  placeholder="(99) 99999-9999"
                  keyboardType="numeric"
                />
                <Blood
                  editable={false}
                  value={route.params.selectedDonate.bloodType}
                  label="Selecionar tipo sanguíneo"
                />
              </Section>
            </KeyboardAwareScrollView>
          </Content>
        </ScrollView>
      </Container>
    </>
  );
};
