import React, {useEffect, useRef} from 'react';
import {FlatList} from 'react-native';
import {useIsFocused} from '@react-navigation/native';
import DropdownAlert from 'react-native-dropdownalert';
import {useDispatch, useSelector} from 'react-redux';
import {Container, Header, Title} from '../../components';
import {
  Section,
  ContainerList,
  SectionTextsList,
  SectionBloodList,
  Blood,
} from './styles';
import Skeleton from './Skeleton';
import {theme} from '../../resources';

import {getSolicitationsById} from '../../redux/solicitation';

export default ({navigation}) => {
  const dropDownAlertRef = useRef();
  const isFocused = useIsFocused();

  const dispatch = useDispatch();
  const {
    isFetchGetSolicitation,
    isSuccessGetSolicitation,
    solicitationsUser,
  } = useSelector((state) => state.solicitation);

  useEffect(() => {
    dispatch(getSolicitationsById());
  }, [isFocused]);

  const renderItem = ({item}) => {
    return (
      <ContainerList
        onPress={() =>
          navigation.navigate('EditSolicitation', {selectedDonate: item})
        }>
        <SectionBloodList>
          <Blood>
            <Title color={theme.secondColor}>{item.bloodType}</Title>
          </Blood>
        </SectionBloodList>
        <SectionTextsList>
          <Title
            numberOfLines={1}
            color={theme.thirdColor}
            align="left"
            marginTop={0}
            marginBottom={0}>
            {item.name}
          </Title>
          <Title color={theme.thirdColor} size="small" align="left">
            {item.address}
          </Title>
        </SectionTextsList>
      </ContainerList>
    );
  };

  return (
    <>
      <Container>
        <Header onPress={() => navigation.goBack()} title="Solicitações" />
        {isSuccessGetSolicitation ? (
          <Section>
            <FlatList
              keyExtractor={(item, index) => index.toString()}
              renderItem={renderItem}
              data={solicitationsUser}
            />
          </Section>
        ) : undefined}

        {isFetchGetSolicitation ? (
          <Section>
            <Skeleton />
          </Section>
        ) : undefined}
      </Container>
      <DropdownAlert ref={dropDownAlertRef} />
    </>
  );
};
