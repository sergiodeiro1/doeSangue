import styled from 'styled-components/native';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';

export const Section = styled.View`
  margin-top: ${hp('4%')}px;
  margin-bottom: ${hp('4%')}px;
`;

export const ContainerList = styled.TouchableOpacity`
  background-color: ${(props) => props.theme.secondColor};
  flex-direction: row;
  align-items: center;
  padding: 12px;
  margin-bottom: 8px;
`;

export const SectionTextsList = styled.View`
  width: 80%;
`;

export const SectionBloodList = styled.View``;

export const Blood = styled.TouchableOpacity`
  margin-right: 16px;
  width: 54px;
  height: 54px;
  border-radius: 29px;
  border-color: ${(props) => props.theme.primaryColor};
  background-color: ${(props) => props.theme.primaryColor};
  border-width: 1px;
  align-items: center;
  justify-content: center;
  margin-top: 8px;
  margin-bottom: 8px;
`;
